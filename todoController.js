var list = document.createElement("ol");
list.id = "list";
list.className = "list";

var doneList = document.createElement("ol");
doneList.id = "list";
doneList.className = "list";
		
var addTask = document.getElementById("inputBtn");
addTask.addEventListener('click', function (){

	var task = document.getElementById("task").value;
	if(task=="") alert("Are you sure you do not have any task to do?");
	else {
		
		addElement (list, doneList, task);
		
		document.getElementById("task").value ="";
	}
}, false);
	
function addElement(ol, olDone, text) {
	
	if(text.trim() !== "") {
		var element = document.createElement("li");
		element.id = "element";
		element.className = "element";
		element.appendChild(document.createTextNode(text));
		ol.appendChild(element);
		document.getElementById("board").appendChild(ol);
		
		addRemoveBtn(element);
		editBtn(element);
		doneBtn(element, olDone, text);
	
	} else {
		alert("You can't create a empty task!");
	}
	
}

function addRemoveBtn(li) {
	
	var removeTask = document.createElement("input");
	removeTask.setAttribute("type", "button");
	removeTask.setAttribute("value", "Remove");
	removeTask.setAttribute("class", "remove");	

	removeTask.addEventListener('click', function (){
		li.parentNode.removeChild(li);
	}, false);
	li.appendChild(removeTask);
}

function editBtn(li) {

	var editTask = document.createElement("input");
	
	editTask.setAttribute("type", "button");
	editTask.setAttribute("value", "Edit");
	editTask.setAttribute("class", "edit");
	editTask.addEventListener('click', function (){
		if(editTask.getAttribute("class")==="edit") {
			editTask.setAttribute("value", "Save");
			editTask.setAttribute("class", "save");
			document.getElementsByName("task")[0].placeholder = "Put here the text to edit!";
		} else if(editTask.getAttribute("class")==="save") {
			var task = document.getElementById("task").value;
			if(task=="") alert("Are you sure you do not have any task to do?");
			else {
				addElement (list, doneList, task);
				document.getElementById("task").value ="";
			}
			document.getElementsByName("task")[0].placeholder = "New task...";
			li.parentNode.removeChild(li);
		}
		
	}, false);
	li.appendChild(editTask);
}
	
function doneBtn(li, ol, text) {
	
		var doneTask = document.createElement("input");
		
		doneTask.setAttribute("type", "button");
		doneTask.setAttribute("value", "Done");
		doneTask.setAttribute("class", "done");
		
		doneTask.addEventListener('click', function (){
			var elementDone = document.createElement("li");
			elementDone.id = "elementDone";
			elementDone.className = "elementDone";
			elementDone.style.textDecoration = "line-through";
			elementDone.style.textDecorationColor = "red";
			addRemoveBtn(elementDone);
			elementDone.appendChild(document.createTextNode(text));
			ol.appendChild(elementDone);
			document.getElementById("doneBoard").appendChild(ol);
			li.parentNode.removeChild(li);
		}, false);
		li.appendChild(doneTask);
	}